FROM node:10

COPY ["package.json", "package-lock.json", "/usr/src/"]

WORKDIR /usr/src

RUN npm install

RUN npm -g install netlify-cli --unsafe-perm

COPY [".","/usr/src/"]

RUN npm run build

EXPOSE 3000

CMD ["npm","start"]
